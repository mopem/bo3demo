﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bo3demo
{
    public partial class Form1 : Form
    {
        private const string ModuleName = "BLACKOPS3";
        private const long WebAddress = 0x14EA0E77A;

        private static int _bytesused;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var bo3 = Process.GetProcessesByName(ModuleName);
            if (bo3.Length == 0)
            {
                MessageBox.Show(@"Game not running!");
                return;
            }

            var sfd = new SaveFileDialog { Filter = @"BO3 Demo Files|*.bo3dem", Title = @"Save a demo" };

            if (sfd.ShowDialog() == DialogResult.OK)
                Task.Factory.StartNew(() => DownloadDemoAsync(sfd.FileName, (int)bo3[0].Handle));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var bo3 = Process.GetProcessesByName(ModuleName);
            if (bo3.Length == 0)
            {
                MessageBox.Show(@"Game not running!");
                return;
            }

            var ofd = new OpenFileDialog { Filter = @"BO3 Demo Files|*.bo3dem", Title = @"Save a demo" };

            if (ofd.ShowDialog() == DialogResult.OK)
                Task.Factory.StartNew(() => InjectDemoAsync(ofd.FileName, (int)bo3[0].Handle));
        }

        private async void InjectDemoAsync(string filename, int handle)
        {
            Invoke((MethodInvoker) delegate { Log("Loading demo..."); });
            try
            {
                var address = DemoAddress();
                var file = File.OpenRead(filename);
                var bytesread = 0;

                do
                {
                    var buffer = new byte[4096];
                    await file.ReadAsync(buffer, 0, 4096);

                    PInvoke.WriteProcessMemory((IntPtr)handle, (IntPtr)address + bytesread, buffer, buffer.Length, out _bytesused);

                    bytesread += _bytesused;

                } while (bytesread < file.Length);

                Invoke((MethodInvoker)delegate { Log("Demo loaded!"); });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Invoke((MethodInvoker)delegate { Log("Loading demo failed: \"" + ex.Message + "\""); });
            }
        }

        private async void DownloadDemoAsync(string filename, int handle)
        {
            try
            {
                var buffer = new byte[1];
                var sb = new StringBuilder();

                for (var i = 0; i < 200; i++)
                {
                    PInvoke.ReadProcessMemory(handle, WebAddress + i, buffer, buffer.Length, ref _bytesused);

                    if (buffer[0] != 0)
                        sb.Append(Encoding.ASCII.GetString(buffer));
                    else
                        break;
                }

                if (sb.ToString() == "")
                {
                    MessageBox.Show(@"No demo loaded");
                    return;
                }
                    
                var wc = new WebClient();

                wc.DownloadProgressChanged += ProgressChanged;
                wc.DownloadFileCompleted += DownloadComplete;

                Invoke((MethodInvoker) delegate { Log(@"Download Started"); });
                await wc.DownloadFileTaskAsync(sb.ToString(), filename);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Invoke((MethodInvoker)delegate { Log("Saving demo failed: \"" + ex.Message + "\""); });
            }
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            Invoke((MethodInvoker) delegate { progressBar1.Value = e.ProgressPercentage; });
        }

        private void DownloadComplete(object sender, AsyncCompletedEventArgs e)
        {
            Invoke((MethodInvoker) delegate { progressBar1.Value = 0; });
            Invoke((MethodInvoker) delegate { Log(@"Download Complete"); });
        }

        private static long DemoAddress()
        {
            var bo3 = Process.GetProcessesByName(ModuleName);
            if (bo3.Length == 0)
            {
                MessageBox.Show(@"Game not running!");
                return 0;
            }

            const long pointer = 0x1427ADC18;
            var buffer = new byte[8];

            PInvoke.ReadProcessMemory((int)bo3[0].Handle, pointer, buffer, buffer.Length, ref _bytesused);

            PInvoke.ReadProcessMemory((int)bo3[0].Handle, BitConverter.ToInt64(buffer, 0) + 0x30, buffer, buffer.Length, ref _bytesused);

            return BitConverter.ToInt64(buffer, 0);
        }

        private void Log(string s, object[] args)
        {
            if (textBox1.Text != null)
            {
                textBox1.Text += Environment.NewLine + string.Format(s, args);
            }
            else
            {
                textBox1.Text += string.Format(s, args);
            }
        }

        private void Log(string s)
        {
            if (textBox1.Text != "")
            {
                textBox1.Text += Environment.NewLine + s;
            }
            else
            {
                textBox1.Text += s;
            }
        }

        private bool GameRunning()
        {
            var temp = Process.GetProcessesByName(ModuleName);
            return temp.Length != 0;
        }
    }

    internal static class PInvoke
    {
        [Flags]
        public enum AllocationType
        {
            Commit = 0x1000,
            Reserve = 0x2000,
            Decommit = 0x4000,
            Release = 0x8000,
            Reset = 0x80000,
            Physical = 0x400000,
            TopDown = 0x100000,
            WriteWatch = 0x200000,
            LargePages = 0x20000000
        }

        [Flags]
        public enum MemoryProtection
        {
            Execute = 0x10,
            ExecuteRead = 0x20,
            ExecuteReadWrite = 0x40,
            ExecuteWriteCopy = 0x80,
            NoAccess = 0x01,
            ReadOnly = 0x02,
            ReadWrite = 0x04,
            WriteCopy = 0x08,
            GuardModifierflag = 0x100,
            NoCacheModifierflag = 0x200,
            WriteCombineModifierflag = 0x400
        }

        [Flags]
        public enum FreeType
        {
            Decommit = 0x4000,
            Release = 0x8000,
        }

        [Flags]
        public enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VirtualMemoryOperation = 0x00000008,
            VirtualMemoryRead = 0x00000010,
            VirtualMemoryWrite = 0x00000020,
            DuplicateHandle = 0x00000040,
            CreateProcess = 0x000000080,
            SetQuota = 0x00000100,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            QueryLimitedInformation = 0x00001000,
            Synchronize = 0x00100000
        }

        [DllImport("kernel32.dll")]
        static extern IntPtr OpenProcess(ProcessAccessFlags processAccess, bool bInheritHandle, int processId);
        public static IntPtr OpenProcess(Process proc, ProcessAccessFlags flags)
        {
            return OpenProcess(flags, false, proc.Id);
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, int nSize, out int lpNumberOfBytesWritten);

        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        public static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, uint dwSize, AllocationType flAllocationType, MemoryProtection flProtect);

        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        public static extern bool VirtualFreeEx(IntPtr hProcess, IntPtr lpAddress, int dwSize, FreeType dwFreeType);

        [DllImport("kernel32")]
        public static extern IntPtr CreateRemoteThread(IntPtr hProcess, IntPtr lpThreadAttributes, uint dwStackSize, IntPtr lpStartAddress, IntPtr lpParameter, uint dwCreationFlags, out IntPtr lpThreadId);

        [DllImport("kernel32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        public static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport("kernel32.dll")]
        public static extern bool ReadProcessMemory(int hProcess, long lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);
    }
}
